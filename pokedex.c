// Copyright (c) Thomas French

// Assignment 2 19T1 COMP1511: Pokedex
// pokedex.c
//
// This program was written by Thomas French (z5206283)
// on 1st of May, 2019
//
// Version 1.0.0: Assignment released.
// Version 1.0.1: Minor clarifications about `struct pokenode`.

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

// Add any extra #includes your code needs here.
#include <string.h>
// But note you are not permitted to use functions from string.h
// so do not #include <string.h>

//#include "pokedex.h"

// Add your own #defines here.

#define TRUE 1
#define FALSE 0

// Note you are not permitted to use arrays in struct pokedex,
// you must use a linked list.
//
// You are also not permitted to use arrays in any of the functions
// below.
//
// The only exception is that char arrays are permitted for
// search_pokemon and functions it may call.
//
// You will need to add fields to struct pokedex.
// You may change or delete the head field.

struct pokedex {
    struct pokenode *head;
    struct pokenode *selected;

};


// You don't have to use the provided struct pokenode, you are free to
// make your own struct instead.
// If you use the provided struct pokenode, you will need to add fields
// to it to store other information.

struct pokenode {
    struct pokenode *next;
    struct pokenode *forward_evolution;
    int found;
    Pokemon         pokemon;
};

// Add any other structs you define here.


// Add prototypes for any extra functions you create here.
int next_min_found_id(Pokedex pokedex, int previous);
int contains_string(char *s, char *search_string);
char *final_character(char *search_string);
struct pokenode *find_second_last_node(Pokedex pokedex);
struct pokenode *find_pokemon_before_selected(Pokedex pokedex);
void add_found_pokemon(Pokedex pokedex, Pokemon pokemon);
void allocation_error(void);
void add_node_specs(struct pokenode *p);
void matching_id_error(void);
void no_registered_pokemon(void);
void print_name_asterisks(char *s);
void detail_not_found(struct pokenode *p);
void detail_found(struct pokenode *p);
int is_type_none_type(pokemon_type type);
void print_found(struct pokenode *p);
void print_not_found(struct pokenode *p);
void constraint_error(void);
void evolve_error(void);
void print_basic_evolution(struct pokenode *p);
void print_evolutions(struct pokenode *p);
int is_pokedex_within_constraints(struct pokenode *move, int factor);
void walking_in_wild_grass(Pokedex pokedex, struct pokenode *move, 
                           int seed, int how_many, int factor);


// You need to implement the following 20 functions.
// In other words, replace the lines calling fprintf & exit with your code.
// You can find descriptions of what each function should do in pokedex.h


Pokedex new_pokedex(void) {
    Pokedex new_pokedex = malloc(sizeof (struct pokedex));
    assert(new_pokedex != NULL);
    // add your own code here
    new_pokedex->head = NULL;
    return new_pokedex;
}

////////////////////////////////////////////////////////////////////////
//                         Stage 1 Functions                          //
////////////////////////////////////////////////////////////////////////

void add_pokemon(Pokedex pokedex, Pokemon pokemon) {

    struct pokenode *p = malloc(sizeof(struct pokenode));

    if (p == NULL){
        allocation_error();
    }

    //pokedex->head <initialized>

    p->pokemon = pokemon; //adds in pokemon specs
    add_node_specs(p);

    struct pokenode *list_node = pokedex->head;
    
    if (pokedex->head == NULL){ //adding first pokemon
        pokedex->head = p;
        pokedex->selected = pokedex->head;

        add_node_specs(pokedex->head);

    } else { //adding to the end of the list

        int search_id = pokemon_id(p->pokemon);

        while (list_node->next != NULL){ 
            //also need to check for matching id's 

            if (search_id == pokemon_id(list_node->pokemon) ){ 
                matching_id_error();
            }

            list_node = list_node->next;
        }

        if (search_id == pokemon_id(list_node->pokemon) ){ 
            //neccessary for 2nd pokemon added case
            matching_id_error();
        }

        list_node->next = p;

    }

}

void detail_pokemon(Pokedex pokedex) {
    
    //we have a selected pokenode
    struct pokenode *save = pokedex->selected;

    if (pokedex->head == NULL){
        no_registered_pokemon();
    }

    if (save->found == FALSE){ //prints out *********

        detail_not_found(save);

    } else { //we have found this pokemon

        detail_found(save);

    }


}

Pokemon get_current_pokemon(Pokedex pokedex) {

    if (pokedex->head == NULL){
        no_registered_pokemon();
    }

    return pokedex->selected->pokemon;

}

void find_current_pokemon(Pokedex pokedex) {

    if (pokedex->head == NULL){ //no pokemon in pokedex
        pokedex->selected = NULL;
    }

    struct pokenode *save = pokedex->selected;

    if (save != NULL){
        save->found = TRUE;
    }   

}

void print_pokemon(Pokedex pokedex) {

    struct pokenode *move = pokedex->head; //starts at first node

    struct pokenode *save = pokedex->selected; //saves selected pokemon


    //if iteration is null, this will do nothing.
    while (move != NULL){

        if (move == save){  //condition if selected
            printf("--> ");
        } else {
            printf("    ");
        }

        if(move->found){

            print_found(move);

        } else {

            print_not_found(move);

        }

        move = move->next;

    }

}

////////////////////////////////////////////////////////////////////////
//                         Stage 2 Functions                          //
////////////////////////////////////////////////////////////////////////

void next_pokemon(Pokedex pokedex) {

    struct pokenode *move = pokedex->head;

    struct pokenode *save = pokedex->selected;

    while (move != NULL){

        if (move == save && move->next != NULL){
            pokedex->selected = move->next;
            break; //we no longer want to cycle through it.
        }

        move = move->next;
    }
    
}

void prev_pokemon(Pokedex pokedex) {

    struct pokenode *move = pokedex->head;

    if (move == NULL){
        //do nothing
    } else if (pokedex->selected == move){
        //do nothing
    } else {

        if (move->next == NULL){
            //do nothing
        } else {

            while (move->next != NULL){

                if (move->next == pokedex->selected){
                    pokedex->selected = move;
                    break;
                }

                move = move->next;

            }

        }

    }

}

void change_current_pokemon(Pokedex pokedex, int p_id) {

    struct pokenode *move = pokedex->head;

    while (move != NULL){
    
        if (p_id == pokemon_id(move->pokemon)){
            pokedex->selected = move;
            break;
        }

        move = move->next;
    }

}

void remove_pokemon(Pokedex pokedex) {

    if (pokedex->head == NULL){
        //there are no pokemon in the pokedex...do nothing
    } else if(pokedex->head->next == NULL){
        //only one pokemon in the pokedex, which is pokedex->head
        struct pokenode *remove = pokedex->head;
        pokedex->head = NULL;
        pokedex->selected = NULL;

        destroy_pokemon(remove->pokemon);
        free(remove);

    } else if (pokedex->selected->next == NULL){
        //we want to remove last pokemon
        //first find pokemon before last pokemon
        struct pokenode *n = find_second_last_node(pokedex);
        struct pokenode *remove = pokedex->selected;

        n->next = NULL;
        pokedex->selected = n;
        n->forward_evolution = NULL;

        destroy_pokemon(remove->pokemon);
        free(remove);

    } else if (pokedex->selected == pokedex->head){

        struct pokenode *remove = pokedex->head;
        pokedex->head = pokedex->head->next;
        pokedex->selected = pokedex->head;
        
        destroy_pokemon(remove->pokemon);
        free(remove);

    } else {
        //free selected pokemon
        //find pokemon before selected
        
        struct pokenode *n = find_pokemon_before_selected(pokedex);
        struct pokenode *remove = pokedex->selected;

        pokedex->selected = pokedex->selected->next;
        n->next = pokedex->selected;
        n->forward_evolution = NULL;
        
        destroy_pokemon(remove->pokemon);
        free(remove);

    }
}

////////////////////////////////////////////////////////////////////////
//                         Stage 3 Functions                          //
////////////////////////////////////////////////////////////////////////

void go_exploring(Pokedex pokedex, int seed, int factor, int how_many) {

    struct pokenode *move = pokedex->head;

    if (pokedex->head == NULL){
        no_registered_pokemon();
    }

    //first we want to check if there exist any pokemon id within the 
    //constraints of 0 to factor-1;

    if (is_pokedex_within_constraints(move, factor) == FALSE){
        constraint_error();
    }

    //now reset back to starting address and cycle through again
    move = pokedex->head;

    walking_in_wild_grass(pokedex, move, seed, how_many, factor);
    //function that simulates walking in wild grass, finding pokemon

}

int count_found_pokemon(Pokedex pokedex){

    struct pokenode *n = pokedex->head;
    int no_found = 0;
  
    while (n != NULL){
        if (n->found == TRUE){
            no_found++;
        }
        n = n->next;
    }
    return no_found;
}

int count_total_pokemon(Pokedex pokedex) {

    struct pokenode *n = pokedex->head;
    int no_pokemon = 0;

    while (n != NULL){
        no_pokemon++;
        n = n->next;
    }
    return no_pokemon;
}

////////////////////////////////////////////////////////////////////////
//                         Stage 4 Functions                          //
////////////////////////////////////////////////////////////////////////

void add_pokemon_evolution(Pokedex pokedex, int from_id, int to_id) {

    //we want to add an evolution from pokemon id a to pokemon_id b;
    int a = from_id;
    int b = to_id;
    //first we need to check if these id's actually exist.
    struct pokenode *move = pokedex->head;
    
    if (pokedex->head == NULL){
        no_registered_pokemon();
    }

    if (a == b){
        evolve_error();
    }

    struct pokenode *pokemon_a = NULL;
    struct pokenode *pokemon_b = NULL;
    //set to null for error checking

    //we need to loop through to find the pokemon
    int found_a = FALSE;
    int found_b = FALSE;

    while (move != NULL){

        int curr_id = pokemon_id(move->pokemon);

        if (curr_id == a){
            found_a = TRUE;
            pokemon_a = move;
        }

        if (curr_id == b){
            found_b = TRUE;
            pokemon_b = move;
        }

        if (found_a && found_b){
            break; //kills loop
        }

        move = move->next;
    }

    if (found_a && found_b){ //we need to connect the evolution!
        pokemon_a->forward_evolution = pokemon_b;

    } else { //we didn't find either a or b, exit program
        no_registered_pokemon();
    }


}

void show_evolutions(Pokedex pokedex) {

    struct pokenode *save = pokedex->selected;

    //we only need to move forward through evolutions!

    if (save->forward_evolution == NULL){
        //no registered evolutions in selected pokemon
        print_basic_evolution(save);

    } else { //we need to cycle through evolutions!
    
        print_evolutions(save);
            
    }


}

int get_next_evolution(Pokedex pokedex) {

    if (pokedex->head == NULL){
        no_registered_pokemon();
    } 

    struct pokenode *n = pokedex->selected;

    if (n->forward_evolution == NULL){
        return DOES_NOT_EVOLVE;
    }

    return pokemon_id(n->forward_evolution->pokemon);

}

////////////////////////////////////////////////////////////////////////
//                         Stage 5 Functions                          //
////////////////////////////////////////////////////////////////////////

Pokedex get_pokemon_of_type(Pokedex pokedex, pokemon_type type) {

    struct pokedex *type_pokedex = malloc(sizeof(struct pokedex));

    if (type_pokedex == NULL){
        allocation_error();
    }
    type_pokedex->head = NULL;

    //one of the pokemon's types need to match as well has been found

    struct pokenode *move = pokedex->head;

    if (pokedex->head == NULL){
        //no pokemon in pokedex, do nothing to type_pokedex
        return type_pokedex;
    }

    //otherwise

    pokemon_type search_type = type; 

    while (move != NULL){

        pokemon_type curr_type1 = pokemon_first_type(move->pokemon);
        pokemon_type curr_type2 = pokemon_second_type(move->pokemon);

        if ( (curr_type1 == search_type || curr_type2 == search_type)
                && move->found ){
            //we want to add this pokemon to our new pokedex.
            struct pokemon *p = clone_pokemon(move->pokemon);

            add_found_pokemon(type_pokedex, p);
        }


        move = move->next;
    }

    return type_pokedex;

}

Pokedex get_found_pokemon(Pokedex pokedex) {

    struct pokedex *found_pokedex = malloc(sizeof(struct pokedex));

    if (found_pokedex == NULL){
        allocation_error();
    }
    found_pokedex->head = NULL;

    struct pokenode *move = pokedex->head;
    
    if (pokedex->head == NULL){
        no_registered_pokemon();
    }

    int previous_id = 0;
    int target_id = next_min_found_id(pokedex, previous_id);


    while (move != NULL){

        int pass = FALSE;
        int curr_id = pokemon_id(move->pokemon);

        if (curr_id == target_id){
            //add to found_pokedex
            struct pokemon *p = clone_pokemon(move->pokemon);
            add_found_pokemon(found_pokedex, p); 
            pass = TRUE;  
        }

        if (pass){
            //we want to reset the loop
            previous_id = target_id;
            target_id = next_min_found_id(pokedex, previous_id);
            move = pokedex->head;

        } else {
            move = move->next;
        }   



    }

    return found_pokedex;
}

Pokedex search_pokemon(Pokedex pokedex, char *text) {

    struct pokedex *search_pokedex = malloc(sizeof(struct pokedex));

    if (search_pokedex == NULL){
        allocation_error();
    }

    search_pokedex->head = NULL; //initialising value

    struct pokenode *move = pokedex->head;
    char *search_string = text;

    while (move != NULL){

        char *s = pokemon_name(move->pokemon);
        if (move->found && contains_string(s, search_string) ){
            //we want to add this to the pokedex
            struct pokemon *p = clone_pokemon(move->pokemon);
            add_found_pokemon(search_pokedex, p);
        }

        move = move->next;
    }


    return search_pokedex;
}

void destroy_pokedex(Pokedex pokedex) {

    struct pokenode *move = pokedex->head;

    while (move != NULL){
        struct pokenode *n = move;
        move = move->next;
        destroy_pokemon(n->pokemon);
        free(n);
    }

    free(pokedex);

}

//THE FOLLOWING FUNCTIONS ARE ALL HELPER FUNCTIONS CALLED ABOVE

struct pokenode *find_pokemon_before_selected(Pokedex pokedex){

    struct pokenode *p = pokedex->head;
    struct pokenode *n = p;

    while (p != pokedex->selected){
        n = p;
        p = p->next;
    }

    return n;

}

struct pokenode *find_second_last_node(Pokedex pokedex){

    struct pokenode *p = pokedex->head;

    while (p->next->next != NULL){

        p = p->next;
    }

    return p;
}



char *final_character(char *search_string){

    char *p = search_string;

    while (*search_string != '\0'){     
        p = search_string; //stores previous address
        search_string++;
    }

    return p;
}

int next_min_found_id(Pokedex pokedex, int previous){

    struct pokenode *n = pokedex->head;
    int min = 151;

    while (n != NULL){
        int id = pokemon_id(n->pokemon);
        if (id < min && id > previous && n->found){
            min = id;
        }

        n = n->next;
    }

    if (min == 151){
        return 0;
    } else {
        return min;
    }

}

void add_found_pokemon(Pokedex pokedex, Pokemon pokemon){

//same as original add_pokemon function, except found = TRUE
    struct pokenode *p = malloc(sizeof(struct pokenode));

    if (p == NULL){
        fprintf(stdin, "Error: Could not allocate memory!\n");
        exit(EXIT_FAILURE);
    }

    //pokedex->head <initialized>

    p->pokemon = pokemon; //adds in pokemon specs
    p->next = NULL; //because adding to end of list
    p->forward_evolution = NULL;
    p->found = TRUE;

    struct pokenode *list_node = pokedex->head;
    
    if (pokedex->head == NULL){ //adding first pokemon
        pokedex->head = p;
        pokedex->head->forward_evolution = NULL;
        pokedex->selected = pokedex->head;
        pokedex->head->found = TRUE;

    } else { //adding to the end of the list

        int search_id = pokemon_id(p->pokemon);

        while (list_node->next != NULL){ 
            //given first address is not null
            //also need to check that we don't have any sharing
            //pokemon id's.  

            if (search_id == pokemon_id(list_node->pokemon) ){ 
                printf("Error: Existing pokemon contains matching ID!\n");
                exit(EXIT_FAILURE); 
            }

            list_node = list_node->next;
        }

        if (search_id == pokemon_id(list_node->pokemon) ){ 
            //neccessary for 2nd pokemon added case
            printf("Error: An existing pokemon contains matching ID!\n");
            exit(EXIT_FAILURE);
        
        }
        //if next node is null
        list_node->next = p;

    }

}

void allocation_error(void){

    printf("Error: Could not allocate memory!\n");
    exit(EXIT_FAILURE);

}

void add_node_specs(struct pokenode *p){

    p->next = NULL;
    p->found = FALSE;
    p->forward_evolution = NULL;

}

void matching_id_error(void){

    printf("Error: Pokemon in pokedex contains matching ID!\n");
    exit(EXIT_FAILURE);

}

void no_registered_pokemon(void){

    printf("Error: No Pokemon registered in the pokedex!\n");
    exit(EXIT_FAILURE);

}

void print_name_asterisks(char *s){

    while (*s != '\0'){
        printf("*");
        s++;
    }    

    printf("\n");   
}

void detail_not_found(struct pokenode *p){

    printf("Id: %03d\n", pokemon_id(p->pokemon));

    char *s = pokemon_name(p->pokemon);
    char *z = s; //makes a copy

    printf("Name: ");
    print_name_asterisks(z);

    printf("Height: --\n");
    
    printf("Weight: --\n");

    printf("Type: --\n");

}

void detail_found(struct pokenode *p){

    printf("Id: %03d\n", pokemon_id(p->pokemon));
    printf("Name: %s\n", pokemon_name(p->pokemon));
    printf("Height: %.1lfm\n", pokemon_height(p->pokemon));
    printf("Weight: %.1lfkg\n", pokemon_weight(p->pokemon));

    pokemon_type d_type1 = pokemon_first_type(p->pokemon);
    pokemon_type d_type2 = pokemon_second_type(p->pokemon);

    const char *type1 = pokemon_type_to_string(d_type1);
    const char *type2 = pokemon_type_to_string(d_type2);

    printf("Type: %s", type1); //will always have a first type

    if (is_type_none_type(d_type2)){ //checks to see if none type
        printf("\n");
    } else {
        printf(" %s\n", type2);
    }

}

int is_type_none_type(pokemon_type type){

    pokemon_type none_type = pokemon_type_from_string("None");

    if (type == none_type){
        return TRUE;
    } else {
        return FALSE;
    }

}

void print_found(struct pokenode *p){

    printf("#%03d: %s\n", 
    pokemon_id(p->pokemon), 
    pokemon_name(p->pokemon));

}

void print_not_found(struct pokenode *p){

    printf("#%03d: ", pokemon_id(p->pokemon));
    char *s = pokemon_name(p->pokemon);
    print_name_asterisks(s);

}

void constraint_error(void){

    printf("Error: No pokemon registered within constraints!\n");
    exit(EXIT_FAILURE);

}

int is_pokedex_within_constraints(struct pokenode *move, int factor){

    int pokemon_id_exist = FALSE;
    while (move != NULL){ //write into function later
        int id = pokemon_id(move->pokemon);
        if (id >= 0 && id <= (factor-1) ){
            pokemon_id_exist = TRUE;
            break;
        }

        move = move->next;
    }

    return pokemon_id_exist;

}

void walking_in_wild_grass(Pokedex pokedex, struct pokenode *move, 
                           int seed, int how_many, int factor){

    srand(seed);
    for (int i = 0; i<how_many; i++){
    
        int search_id = rand()%factor;
        
        while (move != NULL){

            int curr_id = pokemon_id(move->pokemon);
            if (curr_id == search_id){
        //this means that we have found the random pokemon
        //in the pokedex
                move->found = TRUE;
                break;
            }

            move = move->next;
        }   

        //now it should cycle through it again for 'how_many' times
        move = pokedex->head; //reset back to start
    }

}

void evolve_error(void){

    printf("Error: A Pokemon cannot evolve into itsself!\n");
    exit(EXIT_FAILURE);

}

void print_basic_evolution(struct pokenode *p){

    printf("#%03d", pokemon_id(p->pokemon));
        
    pokemon_type d_type1 = pokemon_first_type(p->pokemon);
    pokemon_type d_type2 = pokemon_second_type(p->pokemon);

    const char *type1 = pokemon_type_to_string(d_type1);
    const char *type2 = pokemon_type_to_string(d_type2);

    if (p->found){
            
        printf(" %s", pokemon_name(p->pokemon));

        printf(" [%s", type1);

        if (is_type_none_type(d_type2)) {
            printf("]\n"); 
        } else {
            printf(", %s]\n", type2);
        }
    } else { //we need to print out ?????
        printf(" ????");
        printf(" [????]\n");
 
    } 

}

void print_evolutions(struct pokenode *p){

    while (p != NULL){
        printf("#%03d", pokemon_id(p->pokemon));

        pokemon_type d_type1 = pokemon_first_type(p->pokemon);
        pokemon_type d_type2 = pokemon_second_type(p->pokemon);

        const char *type1 = pokemon_type_to_string(d_type1);
        const char *type2 = pokemon_type_to_string(d_type2);

        if (p->found){
                
            printf(" %s", pokemon_name(p->pokemon));
            printf(" [%s", type1);

            if (is_type_none_type(d_type2) ){
                printf("]");
            } else {
                printf(", %s]", type2);
            }

            if (p->forward_evolution != NULL){
                printf(" --> ");
            } else {
                printf("\n");
            }

        } else { //pokemon not found

                

            printf(" ????");
            printf(" [????]");


            if (p->forward_evolution != NULL){
                printf(" --> ");
            } else {
                printf("\n");
            }

        }

        p = p->forward_evolution;
    }


}

int contains_string(char *s, char *search_string){
    char *save = search_string;
    char *name = s;
    int CAPITOL1 = 0;
    int CAPITOL2 = 0;
    while (*s!= '\0'){
        CAPITOL1 = 0;
        CAPITOL2 = 0;
        name = s;
        save = search_string;
        if (*name >= 'A' && *name <= 'Z'){
            *name += 32;
            CAPITOL1 = 1;
        }
        if (*save >= 'A' && *save <= 'Z'){
            *save += 32;
            CAPITOL2 = 1;
        }
        while (*name == *save){
            if (CAPITOL1)
                *name -= 32;
            if (CAPITOL2)
                *save -=32;
            CAPITOL1 = 0;
            CAPITOL2 = 0;
            name++;
            save++;
            if (*save == '\0'){
                if (CAPITOL1)
                    *name -= 32;
                if (CAPITOL2)
                    *save -=32;
                return TRUE;
            }
            if (*name >= 'A' && *name <= 'Z'){
                *name += 32;
                CAPITOL1 = 1;
            }
            if (*save >= 'A' && *save <= 'Z'){
                *save += 32;
                CAPITOL2 = 1;
            }
        }
        if (CAPITOL1)
            *name -= 32;
        if (CAPITOL2)
            *save -=32;
        s++;
    }
    return FALSE;
}
